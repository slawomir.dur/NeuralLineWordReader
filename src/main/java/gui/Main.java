package gui;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import org.opencv.core.Core;

import java.net.URL;
import java.util.ConcurrentModificationException;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        URL url = getClass().getClassLoader().getResource("ui.fxml");
        Parent root = FXMLLoader.load(url);

        Scene scene = new Scene(root);//, 300, 275);
        stage.setResizable(false);
        stage.setTitle("ImageTextReader");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }
}
