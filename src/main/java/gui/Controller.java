package gui;

import NeuralNetworkLib.*;
import imgproc.ImageProcessor;
import imgproc.ImageProcessorException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import org.opencv.core.Mat;

public class Controller<T> {

    @FXML
    private ImageView loadedImageView;

    @FXML
    private ImageView partitionedImageView;

    @FXML
    private ProgressBar trainProgressBar;

    @FXML
    private Spinner<Integer> inputsSpinner;

    @FXML
    private ToggleButton layersButton;

    @FXML
    private ChoiceBox<Neuron.ActivationFunction> activationFuncBox;

    @FXML
    private TextField errorField;

    @FXML
    private LineChart<Double,Double> trainingErrorChart;

    @FXML
    private TextArea resultTextArea;

    @FXML
    private Label statusLabel;

    private ArrayList<Mat> letters;

    private NeuralNetwork network;

    private HashMap<ArrayList<Double>, Character> resultMap = new HashMap<>();

    private HashMap<Bits, Character> answerMap = new HashMap<>();

    @FXML
    protected void initialize(){
        this.network = new NeuralNetwork(100, 100, 6, Neuron.ActivationFunction.UNIPOLAR, false);
        this.activationFuncBox.getItems().clear();
        this.activationFuncBox.getItems().addAll(Neuron.ActivationFunction.UNIPOLAR, Neuron.ActivationFunction.BIPOLAR);
        this.activationFuncBox.setValue(Neuron.ActivationFunction.UNIPOLAR);
        this.layersButton.setSelected(this.network.Levels()==3);
        this.inputsSpinner.setEditable(true);
        this.inputsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, this.network.neuronsAtLevel(0)));
    }

    @FXML
    protected void handleSetNetwork(){
        this.network = new NeuralNetwork(100, 100, 6, this.activationFuncBox.getValue(), !this.layersButton.isSelected());
    }

    @FXML
    protected void handleLoadImage(ActionEvent event){
        Window win = Stage.getWindows().stream().filter(Window::isShowing).findFirst().orElse(null);
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(win);
        if (selectedFile!=null){
            try {
                FileInputStream fis = new FileInputStream(selectedFile);
                Image image = new Image(fis);
                loadedImageView.setImage(image);
                partitionImage(image);
            } catch (FileNotFoundException exc) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("File could not be opened.");
                alert.setContentText(exc.getMessage());
                alert.show();
            }
        }
    }

    protected void partitionImage(Image img){
        if (img != null){
            Mat mat = ImageProcessor.image2Mat(img);
            ArrayList<Integer> divLines = ImageProcessor.findDivisionLines(mat);
            Mat divMat = ImageProcessor.getImageWithDivisionLinesPrinted(mat, divLines);
            partitionedImageView.setImage(ImageProcessor.mat2Image(divMat));

            ArrayList<Mat> letters = ImageProcessor.cutoutLettersFromLine(mat);
            ArrayList<Mat> equalSizedLetters = new ArrayList<>();
            letters.forEach((letter)->{ equalSizedLetters.add(ImageProcessor.resizeMat(letter,100,100)); });
            this.letters = equalSizedLetters;
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("No loaded image!");
            alert.setContentText("Please, load image first before calling this function");
            alert.show();
        }
    }

    @FXML
    protected void handleReadTextFromImage(ActionEvent event) throws Exception {
        try {
            TrainingData td = toTrainingData(letters, 10);
            ArrayList<ArrayList<Double>> answers = new ArrayList<>();
            for (int i = 0; i < td.size(); ++i) {
                TrainingVector data = td.getVector(i);
                ArrayList<Double> neuralAnswers = this.network.getAnswer(data.getInputs());
                for (int j = 0; j < neuralAnswers.size(); ++j){
                    double val = round(round(neuralAnswers.get(j),2),2);
                    if (val > 0.5) {
                        val = 1.0;
                    } else if (val <= 0.5) {
                        val = 0.0;
                    } else {
                        throw new Exception("Network insufficiently trained. Try setting a lower error.");
                    }
                    neuralAnswers.set(j, val);
                }
                answers.add(neuralAnswers);
            }
            StringBuilder result = new StringBuilder();
            for (ArrayList<Double> answer: answers){
                Bits b = new Bits(answer);
                //TODO hash is not working
                result.append(this.answerMap.get(b));
            }
            this.resultTextArea.setText(result.toString());
        } catch (ImageProcessorException exception) {
            exception.printStackTrace();
        } catch (NeuralNetworkError exception) {
            exception.printStackTrace();
        }
    }

    private double round(double value, int places){
        return BigDecimal.valueOf(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
    }

    @FXML
    protected void handleResetSettingsToDefault(ActionEvent event){
        initialize();
    }

    @FXML
    protected void handleSimplyTrain(ActionEvent event){
        train(1.0);
    }

    @FXML
    protected void handleTrainWhile(ActionEvent event){
        try {
            Double error = Double.parseDouble(errorField.getText());
            train(error);
        } catch (NumberFormatException exception) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Invalid number format");
            alert.setContentText(exception.getMessage());
            alert.show();
        }
    }

    protected TrainingData toTrainingData(ArrayList<Mat> mats, int divFactor) throws ImageProcessorException {
        TrainingData trainingData = new TrainingData();
        HashMap<Integer, Character> alphaMap = getAlphaMap();
        int count = 0;
        ArrayList<Bits> bits = ValuesGenerator.generateValues(52,6);
        this.answerMap.clear();
        for (int i = 0; i < 52; ++i){
            this.answerMap.put(bits.get(i), alphaMap.get(i));
        }
        ArrayList<ArrayList<Double>> allExpected = new ArrayList<>();
        for (Bits bs : bits){
            allExpected.add(bs.getData());
        }
        for (Mat mat: mats) {
            ArrayList<Mat> subMatrices = ImageProcessor.divideIntoSubMatrices(mat, divFactor);
            ArrayList<Double> fieldsOfLetter = new ArrayList<>();
            for (Mat sub : subMatrices) {
                double blackFactor = ImageProcessor.getBlackPercent(sub);
                fieldsOfLetter.add(blackFactor);
            }
            trainingData.addTrainingVector(fieldsOfLetter, allExpected.get(count));
            ++count;
        }
        return trainingData;
    }

    protected void train(double expectedError){
        if (letters != null) {
                System.out.println("Training...");
                Task<ObservableList<XYChart.Series<Double,Double>>> task = new Task<>(){
                  @Override
                  public ObservableList<XYChart.Series<Double,Double>> call() {
                      try {
                          TrainingData trainingData = toTrainingData(letters, 10);
                          updateMessage("Training data formed");
                          XYChart.Series<Double, Double> series = new XYChart.Series<>();
                          series.setName("Error");
                          ObservableList<XYChart.Series<Double,Double>> errorChartData = FXCollections.observableArrayList();
                          errorChartData.add(series);
                          //manual training
                          if (network.isDrawWeights()) {
                              network.drawWeights();
                          }

                          network.setLastError(100.0);
                          int counter = 0;
                          while (network.getLastError() > expectedError){
                              if (network.isDrawWeights()){
                                  network.trainingSession(trainingData, counter%trainingData.size());
                              } else {
                                  network.trainingSessionRandomVector(trainingData);
                              }
                              if (counter%100==0){
                                  double lastErr = network.getLastError();
                                  network.setLastError(network.calculateError(trainingData));
                                  double decr = lastErr - network.getLastError();
                                  double progress = decr / lastErr * 100;
                                  updateProgress(network.getLastError(), 0.1);
                                  updateMessage("Last training error: "+network.getLastError());
                                  errorChartData.get(0).getData().add(new XYChart.Data<>((double) counter, network.getLastError()));
                              }
                              ++counter;
                          }
                          //end of training
                          HashMap<Integer, Character> alphaMap = getAlphaMap();
                          for (int i = 0; i < trainingData.size(); ++i){
                              resultMap.put(network.getAnswer(trainingData.getVector(i).getInputs()), alphaMap.get(i));
                          }
                          updateMessage("Trained successfully");
                          return errorChartData;
                      } catch (Exception exception) {
                          updateMessage(exception.getMessage());
                      }
                      return null;
                  }
                };
                task.messageProperty().addListener((obs, oldMsg, newMsg)->{
                    if (!oldMsg.equals(newMsg)){
                        Platform.runLater(()-> statusLabel.setText(newMsg));
                    }
                });
                task.progressProperty().addListener((obs, oldProg, newProg)->{
                    if (!oldProg.equals(newProg)){
                        Platform.runLater(()-> trainProgressBar.setProgress(newProg.doubleValue()));
                    }
                });
                task.valueProperty().addListener((obs, oldVal, newVal)-> Platform.runLater(() -> {
                    trainingErrorChart.setData(FXCollections.observableArrayList(newVal));

                }));
            new Thread(task).start();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("No data for training!");
            alert.setContentText("Please load alphabet image.");
            alert.show();
        }
    }

    public static HashMap<Integer, Character> getAlphaMap(){
        HashMap<Integer, Character> alphaMap = new HashMap<>();
        for (int i = 0; i < 52; ++i){
            alphaMap.put(i, (char)((i%2==0)?('A'+i/2):('a'+i/2)));
        }
        return alphaMap;
    }

    public static HashMap<Character, Integer> resultMap(){
        HashMap<Character, Integer> result = new HashMap<>();
        HashMap<Integer, Character> alphaMap = getAlphaMap();
        for (int i = 0; i < 52; ++i){
            result.put(alphaMap.get(i), i);
        }
        return result;
    }

}

class Bits
{
    private ArrayList<Double> data = new ArrayList<>();
    public Bits(ArrayList<Double> data){
        this.data = (ArrayList<Double>) data.clone();
    }

    public Bits(int size){
        for (int i = 0; i < size; ++i){
            this.data.add(0.0);
        }
    }

    public ArrayList<Double> getData() {
        return data;
    }

    private boolean evaluate(ArrayList<Double> other){
        for (Double d: other){
            if (d!=0.0 && d!=1.0)
                return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bits other) {
            if (this.data.size() != other.data.size())
                return false;

            Iterator<Double> thisIt = this.data.iterator();
            Iterator<Double> objIt = other.data.iterator();
            while (thisIt.hasNext() && objIt.hasNext()){
                if (!thisIt.next().equals(objIt.next()))
                    return false;
            }
            return true;
        }
        return false;
    }

    public Bits add(ArrayList<Double> other) throws IllegalArgumentException{
        if (this.data.size() != other.size())
            throw new IllegalArgumentException();
        if (!evaluate(other))
            throw new IllegalArgumentException();

        ArrayList<Double> result = new ArrayList<>(other.size());
        boolean carry = false;

        Iterator<Double> otherIt = other.iterator();
        Iterator<Double> thisIt = this.data.iterator();

        while (otherIt.hasNext() && thisIt.hasNext())
        {
            double ot = otherIt.next();
            double th = thisIt.next();

            if ((ot==0.0 && th==1.0) || (ot==1.0 && th==0.0)){
                if (carry) {
                    result.add(0.0);
                } else {
                    result.add(1.0);
                }
            } else if (ot==0.0 && th==0.0) {
                if (carry) {
                    result.add(1.0);
                    carry = false;
                } else {
                    result.add(0.0);
                }
            } else if (ot==1.0 && th==1.0){
                if (carry) {
                    result.add(1.0);
                } else {
                    result.add(0.0);
                    carry = true;
                }
            }
        }
        return new Bits(result);
    }

    public int toInt(){
        int val = 0;
        int counter = 0;
        for (double value : this.data){
            val += value * Math.pow(2.0, counter);
            ++counter;
        }
        return val;
    }

    @Override
    public int hashCode() {
       return toInt();
    }
}

class ValuesGenerator
{
    public static ArrayList<Bits> generateValues(int number, int size){

        ArrayList<Bits> values = new ArrayList<>();
        Bits bits = new Bits(size);
        ArrayList<Double> one = new ArrayList<>();
        for (int i = 0; i < size; ++i){
            if (i==0) {
                one.add(1.0);
            } else {
                one.add(0.0);
            }
        }
        for (int i = 0; i < number; ++i){
            bits = bits.add(one);
            values.add(bits);
        }
        return values;
    }
}
