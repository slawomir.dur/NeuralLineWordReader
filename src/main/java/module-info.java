module gui {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires neuralnetworklib;
    requires opencv450;
    requires java.desktop;
    requires javafx.swing;
    opens gui ;
}