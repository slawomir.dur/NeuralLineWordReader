package imgproc;

public class ImageProcessorException extends Exception {
    public ImageProcessorException(String msg){
        super(msg);
    }
}
