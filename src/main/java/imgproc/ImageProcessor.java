package imgproc;

import NeuralNetworkLib.TrainingData;
import javafx.scene.image.Image;
import javafx.embed.swing.SwingFXUtils;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

public class ImageProcessor {
    private static boolean isDebugging = true;

    public static void setDebug(boolean isDebug){
        isDebugging = isDebug;
    }

    public static ArrayList<Mat> getImgsFromFolder(final File folderPath) throws ImageProcessorException {
        if (folderPath == null || !folderPath.isDirectory())
            throw new ImageProcessorException("Given path is not a directory");
        ArrayList<Mat> resultMats = new ArrayList<>();
        for (final File file: Objects.requireNonNull(folderPath.listFiles())){
            resultMats.add(Imgcodecs.imread(file.getAbsolutePath()));
        }
        return resultMats;
    }

    public static Mat getImgFromPath(final File file) throws ImageProcessorException {
        if (file == null)
            throw new ImageProcessorException("File is null");
        return Imgcodecs.imread(file.getAbsolutePath());
    }

    public static ArrayList<Mat> divideIntoSubMatrices(Mat mat, int number){
        int rowOffset = mat.rows() / number;
        int colOffset = mat.cols() / number;
        int colEnd = colOffset;
        int colStart = 0;
        ArrayList<Mat> mats = new ArrayList<>();
        for (int i = 0; i < number; ++i){
            int rowEnd = rowOffset;
            int rowStart = 0;
            for (int j = 0; j < number; ++j){
                mats.add(mat.submat(rowStart, rowEnd, colStart, colEnd));
                rowStart += rowOffset;
                rowEnd += rowOffset;
            }
            colStart += colOffset;
            colEnd += colOffset;
        }
        return mats;
    }

    public static double getBlackPercent(Mat mat){
        long blackPxls = 0;
        for (int i = 0; i < mat.rows(); ++i){
            for (int j = 0; j < mat.cols(); ++j){
                double[] set = mat.get(i, j);
                double sum = 0.0;
                for (int k = 0; k < set.length; ++k){
                    sum += set[k];
                }
                if (sum == 0.0){
                    ++blackPxls;
                }
            }
        }
        long pixels = mat.rows()*mat.cols();
        return (double)blackPxls/(double)pixels;
    }

    public static ArrayList<Mat> cutoutLettersFromLine(Mat mat) {
        ArrayList<Integer> divLines = findDivisionLines(mat);
        ArrayList<Mat> letterSectors = divideByDivisionLines(mat, divLines);
        ArrayList<Mat> letters = new ArrayList<>();
        for (Mat letterSector: letterSectors){
            try {
                letters.add(cutoutLetter(letterSector));
            } catch (ImageProcessorException exc) {
                System.err.println(exc.getMessage());
            }
        }
        return letters;
    }

    public static Mat prepImg(Mat mat, int kernelForErosion){
        Mat gray = new Mat();
        Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.erode(gray, gray, Imgproc.getStructuringElement(Imgproc.CV_SHAPE_RECT,new Size(kernelForErosion,kernelForErosion)));
        Imgproc.threshold(gray, gray, 100, 255, Imgproc.THRESH_BINARY);
        return gray;
    }

    public static Rect getLetterBoundingRect(Mat mat) throws ImageProcessorException {
        ArrayList<Integer> xs = new ArrayList<>();
        ArrayList<Integer> ys = new ArrayList<>();

        xs.ensureCapacity(mat.rows());
        ys.ensureCapacity(mat.cols());

        for (int i = 0; i < mat.rows(); ++i){
            for (int j = 0; j < mat.cols(); ++j){
                if (mat.get(i, j)[0] == 0){
                    xs.add(j);
                    ys.add(i);
                }
            }
        }
        if (xs.size() != ys.size()){
            throw new ImageProcessorException("Bad image processing");
        }
        if (xs.isEmpty() || ys.isEmpty()){
            throw new ImageProcessorException("xs or ys are empty");
        }
        int minx = Collections.min(xs);
        int miny = Collections.min(ys);

        int maxx = Collections.max(xs);
        int maxy = Collections.max(ys);


        return new Rect(new Point(minx, miny), new Point(maxx, maxy));
    }

    public static ArrayList<Integer> findDivisionLines(Mat mat){
        Mat preped = prepImg(mat, 1);
        ArrayList<Integer> colsWithoutBlack = new ArrayList<>();
        // get cols without black
        for (int i = 0; i < preped.cols(); ++i) {
            boolean hasBlack = false;
            for (int j = 0; j < preped.rows(); ++j) {
                if (preped.get(j, i)[0]==0){
                    hasBlack = true;
                    break;
                }
            }
            if (!hasBlack){
                colsWithoutBlack.add(i);
            }
        }
        // find consecutive cols and take the middle one
        ArrayList<Integer> singleColsWithoutBlack = new ArrayList<>();
        int prevCol = 0;
        int firstDiff = 0;
        for (int col: colsWithoutBlack){
            if (prevCol+1 != col) {
                singleColsWithoutBlack.add(firstDiff+(prevCol-firstDiff)/2);
                firstDiff = col;
            }
            prevCol = col;
        }
        singleColsWithoutBlack.add(preped.cols());
        return singleColsWithoutBlack;
    }

    public static Mat getImageWithDivisionLinesPrinted(Mat mat, ArrayList<Integer> divisionLines){
        Mat copy = new Mat();
        mat.copyTo(copy);
        for (int line: divisionLines){
            Imgproc.line(copy, new Point(line, 0), new Point(line, copy.rows()), new Scalar(0, 255,0));
        }
        return copy;
    }

    public static ArrayList<Mat> divideByDivisionLines(Mat mat, ArrayList<Integer> divisionLines){
        ArrayList<Mat> sectors = new ArrayList<>();
        int prevLine = 0;
        boolean isFirst = true;
        for (int divLine : divisionLines){
            if (isFirst){
                isFirst = false;
                continue;
            }
            sectors.add(mat.submat(0, mat.rows(), prevLine, divLine));
            prevLine = divLine;
        }
        return sectors;
    }

    public static Mat cutoutLetter(Mat mat) throws ImageProcessorException {
        Mat changed = prepImg(mat, 4);
        Rect boundingRect = getLetterBoundingRect(changed);
        return mat.submat(boundingRect);
    }

    public static void saveImgToFolder(String folderPath, String name, Mat mat) throws ImageProcessorException {
        if (folderPath == null)
            throw new ImageProcessorException("Folder path null");
        if (name == null)
            throw new ImageProcessorException("Name is null");
        if (mat == null)
            throw new ImageProcessorException("Mat is null");
        Imgcodecs.imwrite(folderPath+"\\"+name, mat);
    }

    public static Mat resizeMat(Mat mat, int width, int height){
        Mat nm = new Mat();
        Imgproc.resize(mat, nm, new Size(100, 100));
        Imgproc.cvtColor(nm, nm, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(nm, nm, 100, 255, Imgproc.THRESH_BINARY);
        return nm;
    }

    public static void showImgList(ArrayList<Mat> mats){
        int counter = 0;
        for (Mat mat: mats){
            HighGui.imshow(""+counter, mat);
            ++counter;
        }
        HighGui.waitKey(0);
    }

    public static void showMat(Mat m){
        HighGui.imshow("m", m);
        HighGui.waitKey(0);
        HighGui.destroyWindow("m");
    }

    public static Image mat2Image(Mat mat){
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", mat, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }

    public static Mat image2Mat(Image image){
        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
        return bufferedImage2Mat(bufferedImage);
    }

    private static Mat bufferedImage2Mat(BufferedImage im)
    {
        im = toBufferedImageOfType(im, BufferedImage.TYPE_3BYTE_BGR);

        // Convert INT to BYTE
        //im = new BufferedImage(im.getWidth(), im.getHeight(),BufferedImage.TYPE_3BYTE_BGR);
        // Convert bufferedimage to byte array
        byte[] pixels = ((DataBufferByte) im.getRaster().getDataBuffer()).getData();

        // Create a Matrix the same size of image
        Mat image = new Mat(im.getHeight(), im.getWidth(), CvType.CV_8UC3);
        // Fill Matrix with image values
        image.put(0, 0, pixels);

        return image;
    }

    private static BufferedImage toBufferedImageOfType(BufferedImage original, int type) {
        if (original == null) {
            throw new IllegalArgumentException("original == null");
        }

        // Don't convert if it already has correct type
        if (original.getType() == type) {
            return original;
        }

        // Create a buffered image
        BufferedImage image = new BufferedImage(original.getWidth(), original.getHeight(), type);

        // Draw the image onto the new buffer
        Graphics2D g = image.createGraphics();
        try {
            g.setComposite(AlphaComposite.Src);
            g.drawImage(original, 0, 0, null);
        }
        finally {
            g.dispose();
        }

        return image;
    }

    public static TrainingData toTrainingData(ArrayList<Mat> mats, int divFactor) throws ImageProcessorException {
        TrainingData trainingData = new TrainingData();
        HashMap<Integer, Character> alphaMap = getAlphaMap();
        //TODO this is to be fixed; look line below
        if (alphaMap.size()!=mats.size()+1)
            throw new ImageProcessorException("Mats number must be " + alphaMap.size());
        int count = 0;
        for (Mat mat: mats) {
            ArrayList<Mat> subMatrices = ImageProcessor.divideIntoSubMatrices(mat, divFactor);
            ArrayList<Double> fieldsOfLetter = new ArrayList<>();
            for (Mat sub : subMatrices) {
                double blackFactor = ImageProcessor.getBlackPercent(sub);
                fieldsOfLetter.add(blackFactor);
            }
            ArrayList<Double> expected = new ArrayList<>();
            for (int i = 0; i < 52; ++i){
                if (i==count){
                    expected.add(1.0);
                } else {
                    expected.add(0.0);
                }
            }
            trainingData.addTrainingVector(fieldsOfLetter, expected);
        }
        return trainingData;
    }

    public static HashMap<Integer, Character> getAlphaMap(){
        HashMap<Integer, Character> alphaMap = new HashMap<>();
        for (int i = 0; i < 52; ++i){
            alphaMap.put(i, (char)((i%2==0)?('A'+i/2):('a'+i/2)));
        }
        return alphaMap;
    }
}

